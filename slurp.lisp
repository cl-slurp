(defpackage "SLURP"
  (:use "CL")
  (:export "SLURP"
           "STREAM-LENGTH"))

(in-package "SLURP")

(defun stream-length (stream)
  "Returns the length of STREAM, or NIL if the length can't be determined."
  (check-type stream stream)
  (handler-case (file-length stream)
    (type-error () nil)))

(defgeneric slurp (arg &key element-type))

(defmethod slurp ((string string) &key (element-type 'character))
  (slurp (parse-namestring string) :element-type element-type))

(defmethod slurp ((pathname pathname) &key (element-type 'character))
  (with-open-file (stream pathname :direction :input :element-type element-type)
    (slurp stream)))

(defmethod slurp ((stream stream) &key element-type)
  (declare (ignore element-type))
  (let ((stream-element-type (stream-element-type stream)))
    (cond ((subtypep stream-element-type 'character)
           (coerce (loop for c = (read-char stream nil nil)
                         while c
                         collect c) 'string))

          ((subtypep stream-element-type 'unsigned-byte)
           (let ((stream-length (stream-length stream)))
             (if stream-length
                 (let* ((seq (make-array stream-length :element-type stream-element-type))
                        (seq-length (read-sequence seq stream)))
                   (if (= seq-length stream-length)
                       seq
                       (subseq seq 0 seq-length)))
                 (let ((byte-list (loop for b = (read-byte stream nil nil)
                                        while b
                                        collect b)))
                   (make-array (length byte-list) :element-type stream-element-type :initial-contents byte-list)))))

          (t (error "Unexpected stream element type ~S." stream-element-type)))))
